# formspec_anim

A mod library that enables animated formspecs on Minetest 5.0.0+.

## Dependencies

 - [formspec_ast](https://git.minetest.land/luk3yx/formspec_ast) - This allows
    `animated_image[]` tags to be parsed.
 - [sscsm](https://git.minetest.land/luk3yx/sscsm/) - This is needed to draw
    individual frames on the client. This also needs to be installed on clients.

## Usage

This mod adds a new formspec element: `animated_image[]`. This is compatible
with usage described on the similar but different
https://github.com/minetest/minetest/pull/9032, however with more features:

### `animated_image[<X>,<Y>;<W>,<H>;<texture name>:<frame count>,<frame duration>;<oneshot>]`

 - Shows an animated image.
 - `<texture name>` is the name of the texture to use. If this contains a
    single `%s`, that `%s` will be replaced by the current frame, otherwise
    `_<current frame>.png` will be appended to the base texture name. Both `:`
    and `,` are allowed in this field. Note that frame numbers start from `1`.
 - `<frame count>` is the number of frames to display.
 - `<frame duration>` is the duration each frame will be displayed for in
    milliseconds.
 - `<oneshot>`, if `true`, prevents the animation from looping (returning to
    the first frame) when it has reached the last frame. If this parameter is
    omitted, it is assumed to be `false`.

### Notes

 - Using `animated_image[]` elements requires the usage of
    `formspec_anim.show_formspec()` instead of `minetest.show_formspec()`.
    Formspecs are passed through `formspec_ast.interpret()`, and custom
    elements such as `formspec_ast:centered_label[]` should also work.
 - Because of the way formspec_anim works, malicious CSMs could intercept and
    automatically submit formspecs displayed using
    `formspec_anim.show_formspec()`.
 - Interactive elements such as `button[]` and `field[]` (except `list[]`)
    will not work nicely while animations are being displayed and will be
    "reset" each time a new animation frame is drawn.
 - If the `sscsm` CSM isn't installed on clients, only the first frame (or last
    frame for one-shot animations) of the animation will be displayed.

## Testing

The `/formspec_anim` command can be used to show an animated image, and
`/formspec_anim oneshot` can be used to display a one-shot animation.
